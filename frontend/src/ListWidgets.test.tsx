import { render, screen } from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { api } from "./request/http";
import {Widget} from  "./ListWidgets";
import ListWidgets from "./ListWidgets";

const server = setupServer(
  rest.post(`${api()}/widget`, (_req, res, ctx) => {
    return res(
      ctx.delay(200),
      ctx.status(202, "mocked request"),
      ctx.json([
        {
          id: 0,
          name: "test widget 1",
          color: "blue",
          value: 50,
          date_modified: "",
        },
        {
          id: 1,
          name: "test widget 2",
          color: "red",
          value: 75,
          date_modified: "",
        },
        {
          id: 3,
          name: "test widget 3",
          color: "yellow",
          value: 100,
          date_modified: "",
        },
      ])
    );
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("fetch previous widgets", async () => {
	let widgets: Widget[] = [
        {
          id: 0,
          name: "test widget 1",
          color: "blue",
          value: 50,
          date_modified: new Date(),
        },
        {
          id: 1,
          name: "test widget 2",
          color: "red",
          value: 75,
          date_modified: new Date(),
        },
        {
          id: 3,
          name: "test widget 3",
          color: "yellow",
          value: 100,
          date_modified: new Date(),
        },
      ]

  render(<ListWidgets widgets={widgets}/>);

  expect(await screen.findByText("test widget 1")).toBeInTheDocument();
  expect(await screen.findByText("red")).toBeInTheDocument();
  expect(await screen.findByText("value: 100")).toBeInTheDocument();
});
