import { useEffect, useState } from "react";
import CreateWidget from "./CreateWidget";
import { Widget } from "./ListWidgets";
import ListWidgets from "./ListWidgets";
import { api, postData } from "./request/http";

function App() {
  const [widgets, setWidget] = useState<Widget[]>([]);

  //add existing widgets in bulk
  //const AddPreviousWidgets = (prev: Widget[]) => {
  //	prev.map((widget) => {
  //		UpdateWidgets(widget)
  //	})
  //}

  //add a single widget
  const UpdateWidgets = (w: Widget) => {
    setWidget((widgets) => [...widgets, w]);
  };

  //startup query
  useEffect(() => {
    getWidgets();
  }, []);

  //get all previous widgets
  const getWidgets = async () => {
    const res = await postData(`${api()}/widget`).catch((e: any) => {
      console.log("couldn't get widgets: ", e.message);
    });

    if (res) {
      addWidgets(res);
    }
  };

  //add widgets from http response of our backend api
  const addWidgets = (res: any) => {
    let widgetMap = res.map(
      (w: any) =>
        ({
          id: w.id,
          name: w.name,
          color: w.color,
          value: w.value,
          date_modified: w.date_modified,
        } as Widget)
    );

    setWidget(widgetMap);
  };

  return (
    <div className="main-container">
      <div className="main-left">
        <div className="col-left">
          <CreateWidget callback={UpdateWidgets} />
        </div>
      </div>

      <div className="main-right">
        <div className="col-right">
          <ListWidgets widgets={widgets} />
        </div>
      </div>
    </div>
  );
}

export default App;
