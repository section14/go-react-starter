import { useEffect, useState } from "react";
import { api, postData } from "./request/http";
import { Widget } from "./ListWidgets"

const CreateWidget = (props: { callback: (w: Widget) => void }) => {
  const [widgetData, setWidget] = useState<Widget>({
		id: 1,
    name: "",
    color: "",
    value: 0,
		date_modified: new Date(),
  });

  const [colors, setColor] = useState<string[]>([]);

  useEffect(() => {
    const vals = ["red", "blue", "yellow"];
    setColor((colors) => [...colors, ...vals]);
    setColor((colors) => [...colors, "green"]);
  }, []);

  //handle form change
  const handleStringChange = (e: any) => {
    const { name, value } = e.currentTarget;
    setWidget({ ...widgetData, [name]: value });
  };

  const handleNumberChange = (e: any) => {
    const { name, value } = e.currentTarget;
    setWidget({ ...widgetData, [name]: parseInt(value) });
  };

  //create a new widget
  const create = async () => {
    let res = await postData(`${api()}/widget/create`, widgetData).catch(
      (e: any) => {
        console.log("error creating widget", e.message);
        return;
      }
    );

    //update list
    if (res !== undefined) {
      props.callback(widgetData);
    }
  };

  return (
    <div className="col">
      <h1 className="row">Create Widget</h1>
      <div>
        <div className="col text-light">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            onChange={(e) => handleStringChange(e)}
          />
        </div>

        <div className="col mt-3 text-light">
          <label htmlFor="color">Colors</label>

          <select
            name="color"
            id="color"
            onChange={(e) => handleStringChange(e)}
          >
            {colors.map((color, i) => (
              <option key={i} value={color}>
                {color}
              </option>
            ))}
          </select>
        </div>

        <div className="col mt-3 text-light">
          <label htmlFor="value">Value</label>
          <input
            type="number"
            name="value"
            id="value"
            onChange={(e) => handleNumberChange(e)}
          />
        </div>

        <button onClick={() => create()} className="btn mt-3 btn-primary">
          Create
        </button>
      </div>
    </div>
  );
};

export default CreateWidget;
