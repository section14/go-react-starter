import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { api } from "./request/http";
import { Widget } from "./ListWidgets";
import CreateWidget from "./CreateWidget";

const server = setupServer(
  rest.post(`${api()}/widget/create`, (_req, res, ctx) => {
    return res(ctx.delay(200), ctx.status(200, "mocked request"), ctx.json({}));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("create new widget", async () => {
  //mock callback
  const cb = jest.fn();

  //this needs a callback which triggers a re-fetch in ListWidgets
  render(<CreateWidget callback={cb} />);

  //test form input
  const nameInput = screen.getByLabelText("Name") as HTMLInputElement;
  fireEvent.change(nameInput, { target: { value: "Bob" } });
  expect(nameInput.value).toBe("Bob");

  const colorsInput = screen.getByLabelText("Colors") as HTMLSelectElement;
  fireEvent.change(colorsInput, { target: { value: "blue" } });
  expect(colorsInput.value).toBe("blue");

  const valueInput = screen.getByLabelText("Value") as HTMLInputElement;
  fireEvent.change(valueInput, { target: { value: 65 } });
  expect(valueInput.value).toBe("65");

  //click create button and test form data
  userEvent.click(screen.getByRole("button", { name: /Create/i }));

  //test callback
  await waitFor(() => expect(cb).toHaveBeenCalledTimes(1));
});
