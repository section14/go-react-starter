export interface Widget {
  id: number;
  name: string;
  color: string;
  value: number;
  date_modified: Date;
}

/*

Okay, here's what needs to happen.

(done) 1. use the Widget definition in here everywhere (but only one source)
>> 2. modify the backend api to return an id
3. add that id, and a Date() (maybe) to the Widget{} you supplied

4. move the Widgets out of this component, and store them in App.
4a. pass that in as a prop into here

5. now you can use your callback function in CreateWidget to update 
the list

*/

const WidgetItem = (props: { widget: Widget }) => {
  return (
    <>
      <h2>{props.widget.name}</h2>
      <div className="widget-row">
        <div className="row-50 p-2 bg-primary-med">
          color: &nbsp;<strong>{props.widget.color}</strong>
        </div>
        <div className="row-50 p-2">value: {props.widget.value}</div>
      </div>
      <hr />
    </>
  );
};

const ListWidgets = (props: { widgets: Widget[] }) => {
  return (
    <>
      <h1>List Widgets</h1>

      <ul>
        {props.widgets.map((widget, i) => (
          <li key={i}>
            <WidgetItem widget={widget} />
          </li>
        ))}
      </ul>
    </>
  );
};

export default ListWidgets;
