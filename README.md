# Go + React + Postgres Example Project

This example project is a simple web application built with React (16+)
as a frontend, Go (1.16+) as a backend API, and a Postgres database. This project
is using docker-compose to build and run containers for the frontend and backend.

I've created this to be as simple as possible, while still demonstrating how to structure
a project using these tools.

Although I use the term "web application", I'm using this project structure
to build internal, browser based business applications. 

## Requirements:
- Go 1.16 or later
- React 16 or later
- Postgres 9.x
- NPM (I would use nvm)
- NPX (optionally)
- Docker + Docker Compose

## Installation:
- Clone this repository
- Create a postgres database named `go_react`
- Run the `.sql` file in `/schemas` to setup a widget table
- Run `npm install` in the `/frontend` folder
- Run `go mod tidy` in the `/backend` folder

## Configuration:
- Open `/frontend/.env` (optionally save as `.env.local`)
- Change parameters in `.env` to match your system (this is the backend api address)
- Open `/backend/cmd/api/config.json` and change parameters to match your system

## Docker related config:
- Create a .env file with the following:
- `export DB_PASS=yourpassword`
- `export CONFIG=src/cmd/api/json.config` 
- this will be passed into your backend

## Run the application:
- Type `docker-compose up` in terminal to build the backend and frontend 
- I recommend running this in a tmux instance to keep your eye on logs
while being able to detach and do other things

## Frontend changes
- All frontend work will be rebuilt automatically in a React dev server

## Backend changes
- The backend uses an application called `reflex` to watch for a single
file change which causes a rebuild in the container.
- Open a new terminal window (or, preferably, split your tmux window)
- Navigate to `/backend`
- Enter: `echo ${date} > .build` to rebuild and relaunch the backend
- This could be modified to rebuild on every save, but I find that
to be unnecessary with Go.

## Overview:

**This project lets users create, delete, and fetch "widgets".**

### Frontend

The two main components which drive the user interface are `CreateWidget` and
`ListWidgets`. You can see how they communicate with a parent inside of `App.tsx`.

### Backend

The backend's data flows from a router -> controller -> model. Although I'm using
the terms model and controller, I'm not using a MVC architecture per se. The `model` 
package models database table structure, and provide's CRUD functions to access data.

In this project, the `controller` is mainly parsing http requests and marshaling JSON
to send as a response. In a more robust application, controllers handle: transforming data, 
chaining transactional queries, and more advanced logic. While there are plenty of 
other ways to structure applications, I've had success with this method for small and
medium sized apps.

Finally the `router` is simply handling routes, and passing back whatever the controller
has returned. I think it looks cleaner without much logic in the handlers. And, with all the
routes in one place, I can assess what's happening easier.

In theory, user authentication would be handled in the router by wrapping http handlers
with authentication middleware.

All of this was wrote in decoupled fashion to allow for easier mocking during tests.

### Tests

- Both frontend and backend have unit tests
- Run frontend tests with: `npm test` from the root frontend directory
- Run backend tests with `go test -v ./...` from the root backend directory
- todo: integration tests

## Building for production

Building for production is beyond the scope of this project.













