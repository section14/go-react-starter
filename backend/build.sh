#!/bin/sh

set -e
echo "building backend"
cd src/cmd/api
go build -o api main.go
mv api $BUILDPATH
echo "starting backend"
cd $BUILDPATH
./api
