package config

/*

This package initializes global variables based a config.json file that is placed
in the same directory as the executable. Config files are used to easily change
parameters for development and production environments.
Here's an example of a config.json file:

{
    "location" : "http://192.168.0.28",
    "port" : ":8001",
    "logs" : "~/logs/",
	"db_host" : "192.168.0.28",
	"db_port" : "5432",
    "db_name" : "widgets"
}

This file only exists to inject specific urls, ports, database connection objects, and other
configuration variables that need truly global access. Be extremely selective about
adding anything here.

*/

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"os"
	"strconv"

	"gitlab.com/section14/go-react-starter/backend/internal/logger"
)

type config struct {
	Location string `json:"location"`
	Port     string `json:"port"`
	Logs     string `json:"logs"`
	DbHost   string `json:"db_host"`
	DbPort   string `json:"db_port"`
	DbName   string `json:"db_name"`
}

type Config interface {
	Build(configPath string) error
	GetDb() *sql.DB
	WriteLog(logType logger.LogType, msg string)
	WriteError(logType logger.LogType, msg string, err error)
	GetPort() string
}

type Env struct {
	Db       *sql.DB
	Location string
	Logs     string
	LogType  logger.LogType
	Port     string
}

//private vars
var dbhost string
var dbpassword string
var dbport int

const dbuser = "postgres"

func (env *Env) GetDb() *sql.DB {
	return env.Db
}

func (env *Env) WriteLog(logType logger.LogType, msg string) {
	logger.WriteLog(logType, msg, env.Logs)
}

func (env *Env) WriteError(logType logger.LogType, msg string, err error) {
	logger.WriteError(logType, env.Logs, msg, err)
}

func (env *Env) GetPort() string {
	return env.Port
}

//this reads in values from a json file and proccesses
//or assigns those values to globally available vars
func (env *Env) Build(configPath string) error {
	file, err := os.Open(configPath)
	if err != nil {
		log.Println(err)
	}

	decoder := json.NewDecoder(file)

	var conf config
	err = decoder.Decode(&conf)
	if err != nil {
		log.Println(err)
	}

	//populate global variables
	env.Location = conf.Location
	env.Logs = conf.Logs
	env.Port = conf.Port

	dbhost = conf.DbHost
	dbport, _ = strconv.Atoi(conf.DbPort)

	//set password from environment variable. this can
	//be changed to whichever method you prefer
	val, ok := os.LookupEnv("DB_PASS")
	if !ok {
		fmt.Println("DB_PASS env var not set")
	}

	dbpassword = val

	//connect to db, and set global connection variable
	env.Db = connection(conf.DbName)

	//setup connection limits
	env.Db.SetMaxOpenConns(20)
	env.Db.SetMaxIdleConns(2)

	file.Close()

	return nil
}

func connection(dbName string) *sql.DB {
	//make connection to postgres db with a specified database name
	conn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", dbhost, dbport, dbuser, dbpassword, dbName) //sslmode=verify-full
	db, err := sql.Open("postgres", conn)
	if err != nil {
		panic(err.Error())
	}

	//verify connection
	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}

	return db
}
