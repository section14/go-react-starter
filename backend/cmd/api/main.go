package main

import (
	"gitlab.com/section14/go-react-starter/backend/internal/router"
)

func main() {
	router.StartServer()
}
