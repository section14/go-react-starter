module gitlab.com/section14/go-react-starter/backend

go 1.16

require (
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.4
	gitlab.com/s14-public/logger v1.0.0
)
