package logger

//this package wraps calls to a logger package with
//app specific details

import (
	"fmt"
	"gitlab.com/s14-public/logger"
)

//log vars
type LogType int

const (
	General LogType = 1
	Login   LogType = 2
)

var logList = map[LogType]string{
	General: "general.log",
	Login:   "login.log",
}

func WriteLog(logType LogType, msg, location string) {
	logger.WriteLog(logLocation(logType, location), msg)
}

func WriteError(logType LogType, msg, location string, err error) {
	logger.WriteError(logLocation(logType, location), msg, err)
}

func logLocation(logType LogType, location string) string {
	return fmt.Sprintf("%s%s", location, logList[logType])
}
