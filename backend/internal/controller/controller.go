package controller

import (
	"encoding/json"
	"net/http"
)

func GetRequestBody(r *http.Request) (interface{}, error) {
	//This reads the request into an interface type, which needs
	//asserted later on. This mostly serves for reading variable
	//lengths of filterable variables via html form elements.
	//
	//If you are working with a fixed set of request values,
	//you should scan directly into a struct or use the http
	//pkg Request.Form

	//prepare request
	var req interface{}

	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&req)

	if err != nil {
		return nil, err
	}

	return req, nil
}
