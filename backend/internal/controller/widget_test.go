package controller

import (
	"bytes"
	"database/sql"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/section14/go-react-starter/backend/config"
	"gitlab.com/section14/go-react-starter/backend/internal/model"
)

type MockWidgetModel struct {
	Name string
}

func (w *MockWidgetModel) FindAll(r interface{}, env config.Config) ([]model.Widget, error) {
	widgets := []model.Widget{
		{Id: 1,
			Name:         "TestWidget",
			Color:        "Blue",
			Value:        64,
			DateModified: time.Now()},
	}

	return widgets, nil
}

func (w *MockWidgetModel) Find(id int, env config.Config) error {
	wantId := 1

	if id != wantId {
		return errors.New("unknown widget id")
	}

	w.Name = "TestWidget"

	return nil
}

func (w *MockWidgetModel) Create(t *model.Tx) (int64, error) {
	return 1, nil
}

func (w *MockWidgetModel) Update(t *model.Tx) error {
	return nil
}

func (w *MockWidgetModel) Delete(t *model.Tx, id int) error {
	return nil
}

//tx mock, break this out into it's own test-util package
type MockWidgetTx struct{}

func (t *MockWidgetTx) New(db *sql.DB) {}

func (t *MockWidgetTx) Commit() error {
	return nil
}

func (t *MockWidgetTx) Rollback() error {
	return nil
}

func (t *MockWidgetTx) Begin() error {
	return nil
}

func (t *MockWidgetTx) Get() *model.Tx {
	return &model.Tx{}
}

//mock config too. this also needs broke out

func TestGetWidgets(t *testing.T) {
	//mock
	env := &config.Env{}
	service := &MockWidgetModel{}
	tx := &MockWidgetTx{}
	cont := NewWidgetController(service, tx, env)

	t.Run("test valid input", func(t *testing.T) {
		//request
		jsonStr := []byte(`{"name":"TestWidget"}`)
		req := httptest.NewRequest(http.MethodPost, "/api/widget", bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")

		//
		res, err := cont.GetWidgets(req)

		if len(res) == 0 {
			t.Error("response is empty, should be something")
		}

		if err != nil {
			t.Error(err)
		}
	})

	t.Run("test invalid input", func(t *testing.T) {
		//request
		jsonStr := []byte(``)
		req := httptest.NewRequest(http.MethodPost, "/api/widget", bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")

		//
		res, err := cont.GetWidgets(req)

		if len(res) > 0 {
			t.Error("response should be empty without any input")
		}

		if err == nil {
			t.Error("empty requests should return an error")
		}
	})
}

//todo: modify API to take ints only
func TestGetWidget(t *testing.T) {
	//mock
	env := &config.Env{}
	service := &MockWidgetModel{}
	tx := &MockWidgetTx{}
	cont := NewWidgetController(service, tx, env)

	t.Run("test correct id input", func(t *testing.T) {
		res, err := cont.GetWidget("1")

		if len(res) == 0 {
			t.Error("response should not be empty")
		}

		if err != nil {
			t.Error(err)
		}
	})

	t.Run("test incorrect id input", func(t *testing.T) {
		res, err := cont.GetWidget("2")

		if len(res) > 0 {
			t.Error("response should be empty")
		}

		if err == nil {
			t.Error("there should be an error with invalid id input")
		}
	})

	t.Run("test empty id input", func(t *testing.T) {
		res, err := cont.GetWidget("")

		if len(res) > 0 {
			t.Error("response should be empty")
		}

		if err == nil {
			t.Error("there should be an error with invalid id input")
		}
	})
}

func TestCreateWidget(t *testing.T) {
	//mock
	env := &config.Env{}
	service := &MockWidgetModel{}
	tx := &MockWidgetTx{}
	cont := NewWidgetController(service, tx, env)

	//request
	jsonStr := []byte(`{"name":"TestWidget", "color":"Red", "value":5 }`)
	req := httptest.NewRequest(http.MethodPost, "/api/widget/create",
		bytes.NewBuffer(jsonStr))

	req.Header.Set("Content-Type", "application/json")

	t.Run("test create widget with valid info", func(t *testing.T) {
		_, err := cont.CreateWidget(req)

		if err != nil {
			t.Error(err)
		}
	})
}

func TestDeleteWidget(t *testing.T) {
	//mock
	env := &config.Env{}
	service := &MockWidgetModel{}
	tx := &MockWidgetTx{}
	cont := NewWidgetController(service, tx, env)

	//request
	jsonStr := []byte(`{"id":1, "name":"TestWidget", "color":"Blue", "value":5 }`)
	req := httptest.NewRequest(http.MethodPut, "/api/widget/update",
		bytes.NewBuffer(jsonStr))

	req.Header.Set("Content-Type", "application/json")

	t.Run("test delete widget with valid info", func(t *testing.T) {
		err := cont.DeleteWidget(req)

		if err != nil {
			t.Error(err)
		}
	})
}
