package model

import (
	"database/sql"
	"strconv"
	"time"
)

type Where interface {
	buildWhereQuery(r interface{}) (string, []interface{})
}

type Txer interface {
	New(db *sql.DB)
	Get() *Tx
	Commit() error
	Rollback() error
	Begin() error
}

//
type Tx struct {
	db *sql.DB
	tx *sql.Tx
}

func (t *Tx) New(db *sql.DB) {
	//t = &Tx{db: db, tx: nil}
	*t = Tx{db: db, tx: nil}
}

func (t *Tx) Get() *Tx {
	return t
}

func (t *Tx) Begin() error {
	tx, err := t.db.Begin() //error here
	if err != nil {
		return err
	}

	//assign new tx
	t.tx = tx

	return nil
}

func (t *Tx) Commit() error {
	err := t.tx.Commit()
	if err != nil {
		err = t.tx.Rollback()
		if err != nil {
			return err
		}

		return err
	}

	return nil
}

func (t *Tx) Rollback() error {
	err := t.tx.Rollback()
	if err != nil {
		return err
	}

	return nil
}

///////////////////////////////////////////////////////
// Utility functions for getting nullable data types //
///////////////////////////////////////////////////////

func MapNullType(r interface{}, dataType string, res interface{}) {
	//map interface to a nullable type to be used with
	//optional where statements in a postgres database
	//
	//dataTypes are: "string", "int"

	if dataType == "int" {
		var nullInt sql.NullInt64

		if r != nil && r != "" && r != "0" {
			nullInt = GetNullInt64FromString(r, true)
		} else {
			nullInt = GetNullInt64(0, true)
		}

		res = &nullInt
	}
}

func GetNullInt64(i int64, b bool) sql.NullInt64 {
	if b {
		return sql.NullInt64{
			Int64: i,
			Valid: true,
		}
	}

	return sql.NullInt64{}
}

func GetNullInt32(i int32, b bool) sql.NullInt32 {
	if b {
		return sql.NullInt32{
			Int32: i,
			Valid: true,
		}
	}

	return sql.NullInt32{}
}

func GetNullIntFromFloat(i interface{}, b bool) sql.NullInt64 {
	if b {
		//first map to float
		f := i.(float64)
		v := int64(f)

		return sql.NullInt64{
			Int64: v,
			Valid: true,
		}
	}

	return sql.NullInt64{}
}

func GetNullInt64FromString(i interface{}, b bool) sql.NullInt64 {
	if b {
		intConv, _ := strconv.ParseInt(i.(string), 10, 64)

		return sql.NullInt64{
			Int64: intConv,
			Valid: true,
		}
	}

	return sql.NullInt64{}
}

func GetNullInt32FromString(i interface{}, b bool) sql.NullInt32 {
	if b {
		intConv, _ := strconv.ParseInt(i.(string), 10, 32)

		return sql.NullInt32{
			Int32: int32(intConv),
			Valid: true,
		}
	}

	return sql.NullInt32{}
}

func GetNullString(s string, b bool) sql.NullString {
	if b {
		return sql.NullString{
			String: s,
			Valid:  true,
		}
	}

	return sql.NullString{}
}

func GetNullBool(s string, b bool) sql.NullBool {
	if b {
		if s == "true" {
			return sql.NullBool{
				Bool:  true,
				Valid: true,
			}
		} else {
			return sql.NullBool{
				Bool:  false,
				Valid: true,
			}
		}
	}

	return sql.NullBool{}
}

func GetNullTime(s string, b bool) sql.NullTime {
	if b {
		timeFormat := "2006-01-02"
		t, _ := time.Parse(timeFormat, s)

		return sql.NullTime{
			Time:  t,
			Valid: true,
		}
	}

	return sql.NullTime{}
}
