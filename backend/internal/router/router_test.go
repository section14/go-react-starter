package router

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/section14/go-react-starter/backend/internal/model"
)

type MockWidget struct{}

func (w *MockWidget) GetWidgets(r *http.Request) ([]byte, error) {
	widgets := []model.Widget{
		{
			Id:           1,
			Name:         "Widge 1",
			Color:        "blue",
			Value:        50,
			DateModified: time.Now(),
		},
		{
			Id:           2,
			Name:         "Widge 2",
			Color:        "red",
			Value:        100,
			DateModified: time.Now(),
		},
	}

	jsonData, err := json.Marshal(widgets)
	if err != nil {
		return []byte{}, err
	}

	return jsonData, nil
}

func (w *MockWidget) GetWidget(idStr string) ([]byte, error) {
	widget := model.Widget{
		Id:           1,
		Name:         "Widge 1",
		Color:        "blue",
		Value:        50,
		DateModified: time.Now(),
	}

	jsonData, err := json.Marshal(widget)
	if err != nil {
		return []byte{}, err
	}

	return jsonData, nil
}

func (w *MockWidget) CreateWidget(r *http.Request) (int, error) {
	return 1, nil
}

func (w *MockWidget) DeleteWidget(r *http.Request) error {
	return nil
}

func TestGetWidgets(t *testing.T) {
	service := &MockWidget{}
	widgetServer := NewWidgetServer(service)

	req := httptest.NewRequest(http.MethodPost, "/api/widget", nil)
	res := httptest.NewRecorder()

	widgetServer.GetWidgets(res, req)

	//get widgets from response
	var widgets []model.Widget
	decoder := json.NewDecoder(res.Body)
	decoder.Decode(&widgets)

	if len(widgets) != 2 {
		t.Errorf("returning array should have had 2 elements, received: %d", len(widgets))
	}

	if widgets[0].Id != 1 {
		t.Errorf("id of first test widget should be 1, got: %d", widgets[0].Id)
	}

	if widgets[1].Color != "red" {
		t.Errorf("color of first test widget should be red, got: %s", widgets[1].Color)
	}

}

func TestGetWidget(t *testing.T) {
	service := &MockWidget{}
	widgetServer := NewWidgetServer(service)

	req := httptest.NewRequest(http.MethodPost, "/api/widget/5", nil)
	res := httptest.NewRecorder()

	widgetServer.GetWidget(res, req)

	//get widget from response
	var widget model.Widget
	decoder := json.NewDecoder(res.Body)
	decoder.Decode(&widget)

	if widget.Id != 1 {
		t.Errorf("id of test widget should be 1, got: %d", widget.Id)
	}

	if widget.Color != "blue" {
		t.Errorf("color of test widget should be blue, got: %s", widget.Color)
	}
}

func TestCreateWidget(t *testing.T) {
	service := &MockWidget{}
	widgetServer := NewWidgetServer(service)

	req := httptest.NewRequest(http.MethodPost, "/api/widget/create", nil)
	res := httptest.NewRecorder()

	widgetServer.CreateWidget(res, req)

	if res.Code != 200 {
		t.Errorf("response code from CreateWidget should be 200, got %d", res.Code)
	}
}

func TestDeleteWidget(t *testing.T) {
	service := &MockWidget{}
	widgetServer := NewWidgetServer(service)

	req := httptest.NewRequest(http.MethodPost, "/api/widget/delete", nil)
	res := httptest.NewRecorder()

	widgetServer.DeleteWidget(res, req)

	if res.Code != 200 {
		t.Errorf("response code from DeleteWidget should be 200, got %d", res.Code)
	}
}
