-- SEQUENCE: public.widget_id_seq

-- DROP SEQUENCE public.widget_id_seq;

CREATE SEQUENCE public.widget_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.widget_id_seq
    OWNER TO postgres;

-- Table: public.widget

-- DROP TABLE public.widget;

CREATE TABLE public.widget
(
    id bigint NOT NULL DEFAULT nextval('widget_id_seq'::regclass),
    name text COLLATE pg_catalog."default" NOT NULL,
    color text COLLATE pg_catalog."default" NOT NULL,
    value bigint NOT NULL,
	date_modified timestamp without time zone NOT NULL,
    CONSTRAINT widget_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER SEQUENCE public.widget_id_seq OWNED BY public.widget.id;

ALTER TABLE public.widget
    OWNER to postgres;

-- Insert test data
INSERT INTO widget(name,color,value,date_modified) VALUES('square', 'red', 1295, now()::timestamp);
INSERT INTO widget(name,color,value,date_modified) VALUES('circle', 'orange', 2500, now()::timestamp);
INSERT INTO widget(name,color,value,date_modified) VALUES('triangle', 'blue', 4295, now()::timestamp);
INSERT INTO widget(name,color,value,date_modified) VALUES('line', 'purple', 30000, now()::timestamp);
